function flatten(element){
    let eleArray=[];
    if(!element || element.length===0)
        return [];
    for(let i=0;i<element.length;i++){
        if(Array.isArray(element[i])){
            const res=flatten(element[i]);
            for(let j=0;j<element[i].length;j++){
                eleArray.push(res[j]);
            }
        }else{
            eleArray.push(element[i]);
        }
    }
    return eleArray;
}

module.exports = flatten;

/*
const item=[1, [2], [3, [[4]]]];
console.log(flatten(item));
*/