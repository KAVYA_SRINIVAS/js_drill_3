const flatten = require('../flatten.js');

const item=[1, [2], [3, [[4]]]];
console.log(flatten(item));

const item1=[[1,2,3,4,5], [8,4], [3, [[4]]]];
console.log(flatten(item1));

console.log(flatten([]));