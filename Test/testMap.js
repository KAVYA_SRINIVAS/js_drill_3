const map=require('../map.js');


function newArr(item,n){
    return item[n];
}

const item1 = [1, 2, 3, 4, 5, 5];
const item2=[];
const item3=['i','n','c','r','e','d','i','b','l','e']

const test1=map(item1,newArr);
console.log(test1);

const test2=map(item2,newArr);
console.log(test2);

const test3=map(item3,newArr);
console.log(test3);