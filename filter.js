function filter(element,filterElements){
    let res=[];
    if(!element || element.length===0){
        return [];
    }
    for(let i=0;i<element.length;i++){
        let flag=false;
        flag=filterElements(element[i],[0,1,2,3,4,5,6,7,8,9,10]);
        if(flag){
            res.push(element[i]);
        }
    }
    return res;
}

module.exports = filter;

/*
const items=[1,2,30,50,6,25];
console.log(filter(items,filterEle));
*/