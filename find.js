function search(n,searchEle){
    if(n===searchEle)
        return true;
}
function find(element,findele,ele){
    if(!ele || !element || element.length===0){
        return [];
    }
    let res = false;
    for(let i=0;i<element.length;i++){
        res=findele(element[i],ele)
        if(res === true)
            return element[i];
    }
    return res;
}

module.exports = find;

/*
const item1 = [1, 2, 3, 4, 5, 5];
console.log(find(item1,search,1))
*/ 