function map(element,arrFunc){
    let itemList=[];
    if(!element || element.length==0)
        //return "Empty Array";
        return [];
    for(let i=0;i<element.length;i++){
        itemList[i]=arrFunc(element,i);
    }
    return itemList;
}

module.exports = map

 
/*
const items = [1, 2, 3, 4, 5, 5];
console.log(map(items,newArr));
*/